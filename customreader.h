
#ifndef CUSTOMREADER_H
#define CUSTOMREADER_H
#include <stdio.h>
#include <inttypes.h>


const char * ReaderConnect(char *readerString);

void Disconnect();

const char * setPower(int power);

int getPower();

const char * setHighestRssi();


const char * getInputPins();

const char * setInputPins(int * inputPins);

const char * getOutputPins();

const char * setOutputPins(int * outputPins);

const char * setOnGPIO(int * inputList);


const char * getGPIO();

const char * setReadPlan(uint8_t antennaCount, uint8_t * antennaList,int timeout);

const char * setAntenna(uint8_t * antennaList);

const char * getAntenna();

const char * getAntennaList();

const char * setTimeout(int timeout);

const char * getTimeout();

const char * setRegion(int regionNum);

const char * getRegion();

const char * getRegionList();

const char * setType(int typeNum);

const char * getType();

const char * getTypeList(); 

#endif
