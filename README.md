## Config env to build packages on Windows

1. Install MinGW
2. Install packages mingw32-base-bin, mingw-developer-toolkit, msys-base-bin, and libraries like 
3. Add <MingwInstallDir>\bin\ to your PATH
4. Run make script in ```ltkc_win32``` 

## Building library

Go to the MercuryAPI directory:
```cd api```
```make```

if build process fails you should follow instructions and install additionaly:

```
xltproc (linux)
readline-dev (linux)
```

## Building package

Go to the project directory and build project

```
cd /home/pi/go/src/thingmagic_go
go build
```
As result you should have new ```thingmagic_go``` file in ```/home/pi/go/src/thingmagic_go``` directory

## Running program 
Simply execute it as binary:
```
./thingmagic_go
```
## Run program in debug mode
