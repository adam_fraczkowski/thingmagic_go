#define TMR_ENABLE_SERIAL_TRANSPORT_NATIVE
#include <stdio.h>
#include <inttypes.h>
#include "api/tm_reader.h"
#include "api/serial_reader_imp.h"
#include "customreader.h"

TMR_Reader r, *rp;
TMR_Status ret;
TMR_Region region = TMR_REGION_OPEN;
TMR_ReadPlan plan;
//uint8_t antennaList[] = {0x01,0x02};
//uint8_t antennaCount = 0x01;
char buffer[8192] = "";

int readPower;
int timeout;

bool connected = false;

const char * ReaderConnect(char *readerString) {
	ret = TMR_setSerialTransport("tcp", &TMR_SR_SerialTransportTcpNativeInit);
	rp = &r;
	ret = TMR_create(rp,readerString);
    if(TMR_SUCCESS != ret) {
		printf("%s",TMR_strerr(rp, ret));
		return TMR_strerr(rp, ret);

    }
    ret = TMR_connect(rp);
    if(TMR_SUCCESS != ret) {
		printf("%s",TMR_strerr(rp, ret));
		return TMR_strerr(rp, ret);

	}
	connected = true;
    return "OK";
}

void Disconnect() {
	TMR_destroy(rp);
}

const char * setPower(int power) {
	 ret = TMR_paramSet(rp, TMR_PARAM_RADIO_READPOWER, &power);
	 if(TMR_SUCCESS != ret) {
		printf("%s",TMR_strerr(rp, ret));
		return TMR_strerr(rp, ret);

	} else {
		return "OK";
	}
}

int getPower() {
	int power;
	ret = TMR_paramGet(rp,TMR_PARAM_RADIO_READPOWER, &power);
	if(TMR_SUCCESS != ret) {
		printf("%s",TMR_strerr(rp, ret));
		return -1;

	} else {
		return power;
	}
}

const char * setHighestRssi() {
	 bool value = true;
	 ret = TMR_paramSet(rp, TMR_PARAM_TAGREADDATA_RECORDHIGHESTRSSI, &value);
	 if(TMR_SUCCESS != ret) {
		printf("%s",TMR_strerr(rp, ret));
		return TMR_strerr(rp, ret);

	} else {
		return "OK";
	}
}


const char * getInputPins() {
	TMR_uint8List inputPins;

	char tempString[16];
	ret = TMR_paramGet(rp,TMR_PARAM_GPIO_INPUTLIST,&inputPins);
	if(TMR_SUCCESS != ret) {
		printf("%s",TMR_strerr(rp, ret));
		return  TMR_strerr(rp, ret);

	} else {
		memset(buffer,0,sizeof(buffer));
		int i = 0;
		strcat(buffer,"[");
		for(i=0;i<inputPins.len;i++) {
			if(i==inputPins.len-1) {
				sprintf(tempString,"%d",inputPins.list[i]);
			} else {
				sprintf(tempString,"%d,",inputPins.list[i]);
			}
			strcat(buffer,tempString);
		}
		strcat(buffer,"]");
		return buffer;
	}
}

const char * setInputPins(int * inputPins) {
	ret = TMR_paramSet(rp,TMR_PARAM_GPIO_INPUTLIST,inputPins);
	if(TMR_SUCCESS != ret) {
		printf("%s",TMR_strerr(rp, ret));
		return TMR_strerr(rp, ret);

	} else {
		return "OK";
	}
}

const char * getOutputPins() {
	TMR_uint8List outputPins;
	char tempString[128];
	ret = TMR_paramGet(rp,TMR_PARAM_GPIO_OUTPUTLIST,&outputPins);
	if(TMR_SUCCESS != ret) {
		printf("%s",TMR_strerr(rp, ret));
		return  TMR_strerr(rp, ret);

	} else {
		memset(buffer,0,sizeof(buffer));
		int i = 0;
		strcat(buffer,"[");
		for(i=0;i<outputPins.len;i++) {
			if(i==outputPins.len-1) {
				sprintf(tempString,"%d",outputPins.list[i]);
			} else {
				sprintf(tempString,"%d,",outputPins.list[i]);
			}
			strcat(buffer,tempString);
		}
		strcat(buffer,"]");
		return buffer;
	}
}

const char * setOutputPins(int * outputPins) {
	ret = TMR_paramSet(rp,TMR_PARAM_GPIO_OUTPUTLIST,outputPins);
	if(TMR_SUCCESS != ret) {
		printf("%s",TMR_strerr(rp, ret));
		return TMR_strerr(rp, ret);

	} else {
		return "OK";
	}
}

const char * setOnGPIO(int * inputList) {
	TMR_GpioPin * state;
	int stateCount = (sizeof(inputList)/sizeof(inputList[0]));
	state = malloc(stateCount * sizeof(*state));
	int i=0;
	for(i=0;i<stateCount;i++) {
		state[i].id = inputList[i];
		state[i].high = true;
	}
	ret = TMR_gpoSet(rp,stateCount,state);
	if(TMR_SUCCESS != ret) {
		printf("%s",TMR_strerr(rp, ret));
		return TMR_strerr(rp, ret);

	} else {
		return "OK";
	}

}

const char * setOffGPIO(int * inputList) {
	TMR_GpioPin * state;
	int stateCount = (sizeof(inputList)/sizeof(inputList[0]));
	state = malloc(stateCount * sizeof(*state));
	int i=0;
	for(i=0;i<stateCount;i++) {
		state[i].id = inputList[i];
		state[i].high = false;
	}
	ret = TMR_gpoSet(rp,stateCount,state);
	if(TMR_SUCCESS != ret) {
		printf("%s",TMR_strerr(rp, ret));
		return TMR_strerr(rp, ret);

	} else {
		return "OK";
	}

}


const char * getGPIO() {
	TMR_GpioPin state[16];
	uint8_t stateCount = (sizeof(state)/sizeof(state[0]));
	int i = 0;
	char temporaryString[16];
	ret = TMR_gpiGet(rp,&stateCount,state);
	if(TMR_SUCCESS != ret) {
		printf("%s",TMR_strerr(rp, ret));
		return TMR_strerr(rp, ret);
	} else {
		memset(buffer,0,sizeof(buffer));
		strcat(buffer,"{");
		for(i=0;i<stateCount;i++) {
			int pinValue;
			if(state[i].high) {
				pinValue = 1;
			} else {
				pinValue = 0;
			}
			if(i==stateCount-1) {
				sprintf(temporaryString,"\"Pin%d\":\"%d\"",i,pinValue);
			} else {
				sprintf(temporaryString,"\"Pin%d\":\"%d\",",i,pinValue);
			}
			strcat(buffer,temporaryString);
		}
		strcat(buffer,"}");
		return buffer;
	}
}

const char * setReadPlan(uint8_t antennaCount, uint8_t * antennaList,int timeout) {
	ret = TMR_RP_init_simple(&plan,antennaCount,antennaList,TMR_TAG_PROTOCOL_GEN2,timeout);
	if(TMR_SUCCESS != ret) {
		printf("%s",TMR_strerr(rp, ret));
		return TMR_strerr(rp, ret);
	}
	ret = TMR_paramSet(rp, TMR_PARAM_READ_PLAN,&plan);
	if(TMR_SUCCESS != ret) {
		printf("%s",TMR_strerr(rp, ret));
		return TMR_strerr(rp, ret);
	}
	ret = TMR_read(rp,500,NULL);
	if(TMR_SUCCESS != ret) {
		printf("%s",TMR_strerr(rp, ret));
		return TMR_strerr(rp, ret);
	}
	memset(buffer,0,sizeof(buffer));
	strcat(buffer,"[");
	while(TMR_SUCCESS == TMR_hasMoreTags(rp)) {
		TMR_TagReadData trd;
		char epcStr[128];
		char tempString[128];
		ret = TMR_getNextTag(rp,&trd);
		TMR_bytesToHex(trd.tag.epc, trd.tag.epcByteCount,epcStr);

		strcat(buffer,"{");
		strcat(buffer,"\"epc\":\"");
		strcat(buffer,epcStr);
		strcat(buffer,"\",");

		strcat(buffer,"\"rssi\":\"");
		sprintf(tempString,"%d",trd.rssi);
		strcat(buffer,tempString);
		strcat(buffer,"\",");

		strcat(buffer,"\"antenna\":\"");
		sprintf(tempString,"%d",trd.antenna);
		strcat(buffer,tempString);
		strcat(buffer,"\",");


		strcat(buffer,"\"frequency\":\"");
		sprintf(tempString,"%d",trd.frequency);
		strcat(buffer,tempString);
		strcat(buffer,"\",");

		uint64_t timestamp = ((uint64_t)trd.timestampHigh<<32) | trd.timestampLow;
		strcat(buffer,"\"timestamp\":\"");
		sprintf(tempString,"%"PRIu64,timestamp);
		strcat(buffer,tempString);
		strcat(buffer,"\",");

		strcat(buffer,"\"phase\":\"");
		sprintf(tempString,"%d",trd.phase);
		strcat(buffer,tempString);
		strcat(buffer,"\",");

		strcat(buffer,"\"protocol\":\"");
		sprintf(tempString,"%d",trd.tag.protocol);
		strcat(buffer,tempString);
		strcat(buffer,"},");
	}
	if(buffer[strlen(buffer)-1]==',') {
		buffer[strlen(buffer)-1] = 0;

	}
	strcat(buffer,"]");
	return buffer;
}

const char * setAntenna(uint8_t * antennaList) {

}

const char * getAntenna() {

}

const char * getAntennaList() {

}

const char * setTimeout(int timeout) {

}

const char * getTimeout() {

}

const char * setRegion(int regionNum) {

}

const char * getRegion() {

}

const char * getRegionList() {

}

const char * setType(int typeNum) {

}

const char * getType() {

}

const char * getTypeList() {

}