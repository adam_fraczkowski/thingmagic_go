package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"
)

/*
//FOR LINUX X64
#cgo CFLAGS: -Iapi/lib/LTK/LTKC/Library -Iapi/lib/LTK/LTKC/Library/LLRP.org -I. -g -fPIC
#cgo LDFLAGS: api/libmercuryapi.a -lreadline -lpthread api/lib/LTK/LTKC/Library/libltkc.a api/lib/LTK/LTKC/Library/LLRP.org/libltkctm.a

#define TMR_ENABLE_SERIAL_TRANSPORT_NATIVE
#include "customreader.h"
#include <stdio.h>
#include <inttypes.h>


*/
import "C"

type RequestStruct struct {
	Param  string   `json:"param"`
	Values []string `json:"values"`
}

func Disconnect() {
	log.Println("Reader disconnected")
	C.Disconnect()

}

func ReaderConnect(retries int, timeout int, uri string) error {
	result := C.ReaderConnect(C.CString(uri))
	if C.GoString(result) == "OK" {
		return nil
	} else {
		if retries == 0 {
			return fmt.Errorf("ReaderConnectError: %s", C.GoString(result))
		}
		time.Sleep(time.Duration(timeout) * time.Second)
		return ReaderConnect(retries-1, timeout, uri)
	}
}

func Handler(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var JSONreq RequestStruct
	err := decoder.Decode(&JSONreq)
	if err != nil {
		fmt.Println("Invalid Param")
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Invalid Param"))
		return
	}
	switch JSONreq.Param {

	case "connect":
		ret, err := strconv.Atoi(JSONreq.Values[0])
		tim, errTim := strconv.Atoi(JSONreq.Values[1])
		if err != nil || errTim != nil {
			fmt.Println("Invalid Param")
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Invalid Param"))
			return
		}
		errReader := ReaderConnect(ret, tim, JSONreq.Values[2])
		if errReader != nil {
			fmt.Println(errReader)
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(errReader.Error()))
			return
		}
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("OK"))
		break

	case "disconnect":
		C.Disconnect()
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("OK"))
		break

	case "read":
		//result := C.Read()
		//w.WriteHeader(http.StatusOK)
		//w.Write([]byte(C.GoString(result)))
		break

	case "setpower":
		break

	case "getpower":
		break

	case "setinputs":
		break

	case "setoutputs":
		break

	case "setgpo":
		break

	case "getgpi":
		break

	case "getinputs":
		break

	case "getoutputs":
		break

	case "setantenna":
		break

	case "getantenna":
		break

	case "setregion":
		break

	case "getregion":
		break

	case "settype":
		break

	case "gettype":
		break

	case "getregions":
		break

	case "gettypes":
		break

	case "settimeout":
		break

	case "gettimeout":
		break

	default:
		break
	}

	return

}

func main() {
	//log.Println("RUNNING APP ON 9092")
	defer Disconnect()
	err := ReaderConnect(1, 1, "tcp://10.1.1.50:6884")
	if err != nil {
		log.Println(err)
	}
	log.Println("EXAMPLE")
	//run http server
	//r := mux.NewRouter()
	//r.HandleFunc("/reader", Handler).Methods("POST")
	//r.HandleFunc("/power").Methods("POST")
	//r.HandleFunc("/inputs").Methods("GET")
	//r.HandleFunc("/inputs").Methods("POST")
	//r.HandleFunc("/outputs").Methods("GET")
	//r.HandleFunc("/outputs").Methods("POST")
	//r.HandleFunc("/antenna").Methods("POST")
	//r.HandleFunc("/timeout").Methods("POST")
	//r.HandleFunc("/read").Methods("GET")
	//http.ListenAndServe(":9092", r)
}
